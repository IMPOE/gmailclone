import React from "react";
import { IconButton } from "@material-ui/core";
import "./Mail.scss";
import {
  ArrowBack,
  CheckCircle,
  Delete,
  Email,
  Error,
  ExitToApp,
  LabelImportant,
  MoreVert,
  MoveToInbox,
  Print,
  UnfoldMore,
  WatchLater,
} from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
const Mail = () => {
  const history = useHistory();
  const selectedMail = useSelector((store) => store.modal.selectedMail);

  return (
    <div className="email">
      <div className="email__tools">
        <div className="email__toolsLeft">
          <IconButton onClick={() => history.push("/")}>
            <ArrowBack />
          </IconButton>
          <IconButton>
            <MoveToInbox />
          </IconButton>
          <IconButton>
            <Error />
          </IconButton>
          <IconButton>
            <Delete />
          </IconButton>
          <IconButton>
            <Email />
          </IconButton>
          <IconButton>
            <WatchLater />
          </IconButton>
          <IconButton>
            <CheckCircle />
          </IconButton>
          <IconButton>
            <LabelImportant />
          </IconButton>
          <IconButton>
            <MoreVert />
          </IconButton>
        </div>
        <div className="email__toolsRight">
          <IconButton>
            <UnfoldMore />
          </IconButton>
          <IconButton>
            <Print />
          </IconButton>
          <IconButton>
            <ExitToApp />
          </IconButton>
        </div>
      </div>
      <div className="email__body">
        <div className="email__bodyHeader">
          <h2>{selectedMail?.subject}</h2>
          <LabelImportant className="email__important" />
          <p>{selectedMail?.title}</p>
          <p className="email__time">{selectedMail?.time}</p>
        </div>
        <div className="email__message">
          <p>{selectedMail?.description}</p>
        </div>
      </div>
    </div>
  );
};

export default Mail;
