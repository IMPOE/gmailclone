import React from "react";
import "./EmailRow.scss";
import { Checkbox, IconButton } from "@material-ui/core";
import { LabelImportantOutlined, StarBorderOutlined } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SELECT_EMAIL } from "../../store/actions/modalAction";
const EmailRow = ({ id, title, description, subject, time }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const OpenEmail = () => {
    dispatch({
      type: SELECT_EMAIL,
      payload: { id, title, description, subject, time },
    });
    history.push("/mail");
  };
  return (
    <div className="emailRow">
      <div className="emailRow__options">
        <Checkbox />
        <IconButton>
          <StarBorderOutlined />
        </IconButton>
        <IconButton>
          <LabelImportantOutlined />
        </IconButton>
      </div>
      <h3 className="emailRow__title" onClick={OpenEmail}>
        {title}
      </h3>
      <div className="emailRow__message" onClick={OpenEmail}>
        <h4 className="emailRow__description">
          {subject} -<span>{description}</span>
        </h4>
      </div>
      <p className="emailRow__time">{time}</p>
    </div>
  );
};

export default EmailRow;
