import React from "react";
import "./sideBarOption.scss";
import { useDispatch } from "react-redux";
const SideBarOption = ({ Icon, title, number, selected, options }) => {
  const dispatch = useDispatch();
  return (
    <div
      className={`sideBar__option ${selected && "sideBar__option-active"}`}
      onClick={() => options && dispatch(options)}
    >
      <Icon />
      <h3>{title}</h3>
      <p>{number}</p>
    </div>
  );
};

export default SideBarOption;
