import React, { useState } from "react";
import "./SideBar.scss";
import { Button, IconButton } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import InboxIcon from "@material-ui/icons/Inbox";
import StartIcon from "@material-ui/icons/Star";
import AccessTimeIcon from "@material-ui/icons/AccessTime";
import LabelImportantIcon from "@material-ui/icons/LabelImportant";
import NearMeIcon from "@material-ui/icons/NearMe";
import NoteIcon from "@material-ui/icons/Note";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import PersonIcon from "@material-ui/icons/Person";
import DuoIcon from "@material-ui/icons/Duo";
import PhoneIcon from "@material-ui/icons/Phone";
import VideocamIcon from "@material-ui/icons/Videocam";
import KeyboardIcon from "@material-ui/icons/Keyboard";
import FormatQuoteIcon from "@material-ui/icons/FormatQuote";
import PermContactCalendarIcon from "@material-ui/icons/PermContactCalendar";
import SmsIcon from "@material-ui/icons/Sms";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import SideBarOption from "./SideBarOption/SideBarOption";
import { useDispatch, useSelector } from "react-redux";
import { modalShowAction } from "../../store/actions/modalAction";
import { Link } from "react-router-dom";
import {
  closeOptionsAction,
  openOptionsAction,
} from "../../store/actions/sideBarAction";

const SideBar = () => {
  const dispatch = useDispatch();
  const options = useSelector((state) => state.sideBar.showOptions);
  const [chatMenu, setChatMenu] = useState("Hangouts");

  return (
    <div className="sideBar">
      <Button
        onClick={() => dispatch(modalShowAction())}
        startIcon={<AddIcon fontSize="large" />}
        className="sideBar__compose"
      >
        Compose
      </Button>
      <div className="sideBar__options-container">
        <SideBarOption
          Icon={InboxIcon}
          title="Inbox"
          number={54}
          selected={true}
        />
        <SideBarOption Icon={StartIcon} title="Starred" number={54} />
        <SideBarOption Icon={AccessTimeIcon} title="Snoozed" number={54} />
        <SideBarOption
          Icon={LabelImportantIcon}
          title="Important"
          number={54}
        />
        <SideBarOption Icon={NearMeIcon} title="Sent" number={54} />
        <SideBarOption Icon={NoteIcon} title="Drafts" number={54} />

        {options && <SideBarOption Icon={SmsIcon} title="Chats" number={54} />}
        {options ? (
          <SideBarOption
            options={closeOptionsAction()}
            Icon={KeyboardArrowUpIcon}
            title="Less"
          />
        ) : (
          <SideBarOption
            options={openOptionsAction()}
            Icon={ExpandMoreIcon}
            title="More"
          />
        )}
      </div>
      <div className="sideBar__meet">
        <h4>Meet</h4>
        <SideBarOption Icon={VideocamIcon} title="New meeting" />
        <SideBarOption Icon={KeyboardIcon} title="Join a meeting" />
      </div>

      {chatMenu === "Contacts" && (
        <div className="sideBar__chats">
          <PermContactCalendarIcon
            color="action"
            className="sideBar__chats-icon"
          />
          <span className="sideBar__chats-recent">No Hangouts contacts</span>
          <Link className="sideBar__chats-newChat" to="/">
            <span>Find someone</span>
          </Link>
        </div>
      )}
      {chatMenu === "Hangouts" && (
        <div className="sideBar__chats">
          <FormatQuoteIcon color="action" className="sideBar__chats-icon" />
          <span className="sideBar__chats-recent">No recent chats</span>
          <Link className="sideBar__chats-newChat" to="/">
            <span>Start a new one</span>
          </Link>
        </div>
      )}
      {chatMenu === "Calls" && (
        <div className="sideBar__chats">
          <PhoneIcon color="action" className="sideBar__chats-icon" />
          <span className="sideBar__chats-recent">
            Call phones feature is not available.
          </span>
          <Link className="sideBar__chats-newChat" to="/">
            <span>Learn More</span>
          </Link>
        </div>
      )}

      <div className="sideBar__footer">
        <div className="sideBar__footerIcons">
          <IconButton onClick={() => setChatMenu("Contacts")}>
            <PersonIcon />
          </IconButton>
          <IconButton onClick={() => setChatMenu("Hangouts")}>
            <DuoIcon />
          </IconButton>
          <IconButton onClick={() => setChatMenu("Calls")}>
            <PhoneIcon />
          </IconButton>
        </div>
      </div>
    </div>
  );
};

export default SideBar;
