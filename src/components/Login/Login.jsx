import React from "react";
import "./Login.scss";
import { Button } from "@material-ui/core";
import { auth, provider } from "../../firebase";
import { useDispatch } from "react-redux";
import { LOGIN } from "../../store/actions/userAction";
const Login = () => {
  const dispatch = useDispatch();

  const SignIn = () => {
    auth
      .signInWithPopup(provider)
      .then(({ user }) => {
        dispatch({
          type: LOGIN,
          payload: {
            displayName: user.displayName,
            mail: user.email,
            photoUrl: user.photoURL,
          },
        });
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  return (
    <div className="login">
      <div className="login__container">
        <img
          src="https://upload.wikimedia.org/wikipedia/commons/7/7e/Gmail_icon_%282020%29.svg"
          alt="GmailIcon"
        />
        <Button variant="contained" color="primary" onClick={SignIn}>
          Login
        </Button>
      </div>
    </div>
  );
};

export default Login;
