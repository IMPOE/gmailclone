import React from "react";
import "./SendMail.scss";
import { Close } from "@material-ui/icons";
import { Button } from "@material-ui/core";
import { useForm } from "react-hook-form";
import { modalHideAction } from "../../store/actions/modalAction";
import { useDispatch } from "react-redux";
import { db } from "../../firebase";
import firebase from "firebase";
const SendMail = () => {
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();

  const OnSubmit = (formData) => {
    console.log(formData);
    db.collection("emails").add({
      to: formData.name,
      subject: formData.subject,
      message: formData.message,
      timestamp: firebase.firestore.FieldValue.serverTimestamp(),
    });
    dispatch(modalHideAction());
  };

  return (
    <div className="sendMail">
      <div className="sendMail__header">
        <h3>New Message</h3>
        <Close
          className="sendMail__close"
          onClick={() => dispatch(modalHideAction())}
        />
      </div>

      <form onSubmit={handleSubmit(OnSubmit)}>
        <input
          name="to"
          placeholder="To"
          type="email"
          {...register("name", { required: true, minLength: 4 })}
        />
        <p className="sendMail__error">
          {errors.name && errors.name.type === "required" && (
            <span>This is required</span>
          )}
        </p>

        <input
          subject="subject"
          placeholder="Subject"
          type="text"
          {...register("subject", { required: true })}
        />
        <p className="sendMail__error">
          {errors.subject && errors.subject.type === "required" && (
            <span>This is required</span>
          )}
        </p>
        <input
          message="message"
          className="sendMail__message"
          placeholder="Message..."
          type="text"
          {...register("message", { required: true })}
        />
        <p className="sendMail__error">
          {" "}
          {errors.message && errors.message.type === "required" && (
            <span>This is required</span>
          )}
        </p>
        <div className="sendMail__options">
          <Button
            variant="contained"
            color="primary"
            type="submit"
            className="sendMail__send"
          >
            Send
          </Button>
        </div>
      </form>
    </div>
  );
};

export default SendMail;
