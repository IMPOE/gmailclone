import React, { useEffect } from "react";
import "./App.scss";
import Header from "./components/Header/Header";
import SideBar from "./components/SideBar/SideBar";
import Mail from "./components/Mail/Mail";
import EmailList from "./components/EmailList/EmailList";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import SendMail from "./components/SendMail/SendMail";
import { useDispatch, useSelector } from "react-redux";
import Login from "./components/Login/Login";
import { auth } from "./firebase";
import { LOGIN } from "./store/actions/userAction";
function App() {
  const sendMassageIsOpen = useSelector(
    (store) => store.modal.sendMessageIsOpen
  );
  const user = useSelector((state) => state.user.user);
  const dispatch = useDispatch();

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      if (user) {
        dispatch({
          type: LOGIN,
          payload: {
            displayName: user.displayName,
            mail: user.email,
            photoUrl: user.photoURL,
          },
        });
      } else {
      }
    });
  }, []);

  return (
    <Router>
      {!user ? (
        <Login />
      ) : (
        <div className="ppp">
          <Header />
          <div className="app__body">
            <SideBar />
            <Switch>
              <Route path="/mail">
                <Mail />
              </Route>
              <Route>
                <EmailList path="/" />
              </Route>
            </Switch>
          </div>
          {sendMassageIsOpen && <SendMail />}
        </div>
      )}
    </Router>
  );
}

export default App;
