import firebase from "firebase";
const firebaseConfig = {
  apiKey: "AIzaSyDAW9G5ak-qArdFrhLYl3JjIed5jQH9xQM",
  authDomain: "clone-87e6f.firebaseapp.com",
  projectId: "clone-87e6f",
  storageBucket: "clone-87e6f.appspot.com",
  messagingSenderId: "685584741661",
  appId: "1:685584741661:web:b0080ed75c4817f9e71df4",
  measurementId: "G-WRZ064R8M3",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export { db, auth, provider };
