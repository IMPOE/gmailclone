import {CLOSE_SEND_MESSAGE, OPEN_SEND_MESSAGE, SELECT_EMAIL} from "../actions/modalAction";


const initialState = {
  selectedMail : null,
  sendMessageIsOpen: false,
};

export const mailReducer = (state = initialState, action) => {
  switch (action.type) {
    case SELECT_EMAIL:
      return {
        selectedMail: action.payload,
      };
    case OPEN_SEND_MESSAGE:
      return {
        sendMessageIsOpen: true,
      };
    case CLOSE_SEND_MESSAGE:
      return {
        sendMessageIsOpen: false,
      };
    default:
      return state;
  }
};
