import {CLOSE_OPTIONS, OPEN_OPTIONS} from "../actions/sideBarAction";
const initialState = {
    showOptions: false,
};

export const sidebarReducer = (state = initialState, action) => {
    switch (action.type) {
        case OPEN_OPTIONS:
            return {
                showOptions: true,
            };
        case CLOSE_OPTIONS:
            return {
                showOptions: false,
            };
        default:
            return state;
    }
};
