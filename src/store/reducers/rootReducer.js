import { combineReducers } from 'redux';
import { mailReducer } from './mailReducer';
import {sidebarReducer} from "./sideBarReducer";
import {userReducer} from "./userReducer";


export const rootReducer = combineReducers({
  user: userReducer,
  modal: mailReducer,
  sideBar: sidebarReducer,
});
