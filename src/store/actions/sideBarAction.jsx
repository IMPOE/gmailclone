export const OPEN_OPTIONS = "OPEN_OPTIONS";
export const CLOSE_OPTIONS = "CLOSE_OPTIONS";

export const openOptionsAction = () => (dispatch) => {
  dispatch({
    type: OPEN_OPTIONS,
    payload: true,
  });
};

export const closeOptionsAction = () => (dispatch) => {
  dispatch({
    type: CLOSE_OPTIONS,
    payload: false,
  });
};
