export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";

export const logout = () => (dispatch) => {
  dispatch({
    type: LOGOUT,
    payload: null,
  });
};
