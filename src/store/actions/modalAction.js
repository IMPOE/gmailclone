export const OPEN_SEND_MESSAGE = "OPEN_SEND_MESSAGE";
export const CLOSE_SEND_MESSAGE = "CLOSE_SEND_MESSAGE";
export const SELECT_EMAIL = "SELECT_EMAIL";
export const modalShowAction = () => (dispatch) => {
  dispatch({
    type: OPEN_SEND_MESSAGE,
    payload: true,
  });
};

export const modalHideAction = () => (dispatch) => {
  dispatch({
    type: CLOSE_SEND_MESSAGE,
    payload: false,
  });
};
